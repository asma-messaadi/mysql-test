

const puppeteer = require('puppeteer');
const fs = require("fs-extra");
const C = require('./constants');
const USERNAME_SELECTOR = 'body > div.content > form.login-form > div:nth-child(2) > input';
const PASSWORD_SELECTOR = 'body > div.content > form.login-form > div:nth-child(3) > input';
const CTA_SELECTOR = 'body > div.content > form.login-form > div.form-actions > button';
const fss = require('fs');
(async function main ()
    {

        try {

          const browser = await puppeteer.launch({headless: false}, {ignoreHTTPSErrors: true});
          const page = await browser.newPage();
          page.setUserAgent('Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1');
          await page.goto ('https://publisher-dev.affluent.io');
          console.log('***************************')
          await page.click(USERNAME_SELECTOR);
          await page.keyboard.type(C.username);
          await page.click(PASSWORD_SELECTOR);
          await page.keyboard.type(C.password);

          await page.click(CTA_SELECTOR);
          await page.waitForNavigation();
          console.log('It is showing');
          const page2 = await browser.newPage();
          await page2.goto('https://publisher-dev.affluent.io/api/query/chart?resolution=day&type=dates&startDate=2019-08-01&endDate=2019-08-31&currency=USD');
          var content = await page2.content(); 
          innerText = await page2.evaluate(() =>  {
                return JSON.parse(document.querySelector("body").innerText); 
         }); 

    console.log("innerText now contains the JSON");
    console.log(innerText); 
    let data = JSON.stringify(innerText);
    fss.writeFileSync('api.json', data); 
      } catch (e){
          console.log('my error', e); 
         }

})();

